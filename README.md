# Commerce Promotion Terms and Conditions

## Introduction
This module provides a checkbox with a modal dialog for letting the user knows
about the promotion terms and conditions for Drupal Commerce.

### Permissions included:
 - administer commerce promotion terms and permissions

## Requirements
- drupal:commerce
- drupal:commerce_promotion

## Installation
Usual drupal module installation.

## Configuration
No configuration needed.

## Usage
A new field named "Terms and Condition" will be shown at the promotion form. If
there this field is filled with some information, one checkbox will appear
during the checkout process just after the coupon usage related to the referring
promotion.
