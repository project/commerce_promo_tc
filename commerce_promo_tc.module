<?php

/**
 * @file
 * Contains commerce_promo_tc.module.
 */

use Drupal\commerce_promo_tc\Entity\CommercePromoTc;
use Drupal\commerce_promotion\Entity\Coupon;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\filter\FilterPluginCollection;

/**
 * Implements hook_help().
 */
function commerce_promo_tc_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the commerce_promo_tc module.
    case 'help.page.commerce_promo_tc':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Adds a field to promotions called "Terms and Conditions" that appears in a custom checkout pane.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function commerce_promo_tc_theme() {
  return [
    'commerce_promo_tc_coupon_redemption_form' => [
      'render element' => 'form',
    ],
  ];
}

/**
 * Implements hook_form_alter().
 */
function commerce_promo_tc_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (in_array($form_id, [
    'commerce_promotion_add_form',
    'commerce_promotion_edit_form',
    'commerce_promotion_duplicate_form',
  ])) {

    $form_obj = $form_state->getFormObject();
    $promotion = $form_obj->getEntity();
    $terms_and_conditions_id = \Drupal::getContainer()->get('database')
      ->query('SELECT id FROM {commerce_promo_tc_field_data} WHERE commerce_promotion_id = :commerce_promotion_id AND langcode = :langcode', [
        ':commerce_promotion_id' => $promotion->id(),
        ':langcode' => $promotion->language()->getId(),
      ])->fetchCol();

    $terms_and_conditions = NULL;
    if (count($terms_and_conditions_id) && array_values($terms_and_conditions_id)[0]) {
      $terms_and_conditions = CommercePromoTc::load(array_values($terms_and_conditions_id)[0]);
    }

    $form['terms_and_conditions'] = [
      '#type' => 'text_format',
      '#title' => t('Terms and Condition'),
      '#description' => t('Terms and Conditions to be accepted when using this promotion.'),
      '#default_value' => $terms_and_conditions && isset($terms_and_conditions->get('value')->getValue()[0]['value']) ? $terms_and_conditions->get('value')->getValue()[0]['value'] : '',
      '#format' => $terms_and_conditions && isset($terms_and_conditions->get('format')->getValue()[0]['value']) ? $terms_and_conditions->get('format')->getValue()[0]['value'] : filter_default_format(),
      '#weight' => 1.1,
    ];

    $submit = 'Drupal\commerce_promo_tc\Form\PromotionTermsAndConditionsForm::submit';
    $form['#submit'][] = $submit;
    $form['actions']['submit']['#submit'][] = $submit;
    $form['actions']['submit_continue']['#submit'][] = $submit;
  }
}

/**
 * Implements hook_commerce_inline_form_PLUGIN_ID_alter().
 */
function commerce_promo_tc_commerce_inline_form_coupon_redemption_alter(array &$inline_form, FormStateInterface $form_state, array &$complete_form) {

  $complete_form['#attached']['library'][] = 'commerce_promo_tc/commerce_promo_tc';
  $complete_form['#validate'][] = 'commerce_promo_tc_form_validate';
  $complete_form['actions']['next']['#submit'][] = 'commerce_promo_tc_order_next_step_form_submit';
  $coupons = &$inline_form['coupons'];
  $inline_form['#theme'] = 'commerce_promo_tc_coupon_redemption_form';
  $tempstore = \Drupal::getContainer()->get('tempstore.private')->get('commerce_promo_tc');
  $tc_accepted_ids = $tempstore->get('tc_accepted');
  foreach ($coupons as $key => $coupon) {
    $coupon_id = $coupon['remove_button']['#coupon_id'];
    $coupon_entity = Coupon::load($coupon_id);
    if ($coupon_entity instanceof Coupon) {
      $promotion = $coupon_entity->getPromotion();
      $tc = CommercePromoTc::getTermsAndConditions($promotion);
      if ($tc instanceof CommercePromoTc && strlen(strip_tags(trim($tc->getText()))) > 0) {
        $coupons[$key]['terms_and_conditions'] = [
          'tc_check' => [
            '#type' => 'checkbox',
            '#title' => t('I have read & accept the %tc', [
              '%tc' => Markup::create('<span class="commerce-promo-tc"><a class="use-ajax" data-dialog-type="modal"  data-dialog-options="{&quot;width&quot;:500}"  href="/commerce-promo-tc/' . $promotion->id() . '">' . t('Terms and Conditions') . '</a></span>'),
            ]),
            '#required' => TRUE,
            '#value' => $tc->id(),
            '#attributes' => [
              'checked' => isset($tc_accepted_ids[$tc->id()]) ? 'checked' : FALSE,
            ],
          ],
        ];
      }
    }
  }
}

/**
 * Additional validator to make sure TC is only required when advancing steps.
 *
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form_state object.
 */
function commerce_promo_tc_form_validate(array $form, FormStateInterface $form_state) {
  $errors = $form_state->getErrors();
  $form_state->clearErrors();
  $trigger = $form_state->getTriggeringElement();

  if ($trigger['#id'] != 'edit-actions-next') {
    unset($errors['coupon_redemption][form][coupons][0][terms_and_conditions][tc_check']);
  }

  foreach ($errors as $name => $error) {
    $form_state->setErrorByName($name, $error);
  }
}

/**
 * Saves TC acceptance for usage when revisiting the coupon page.
 *
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state object.
 *
 * @throws \Drupal\Core\TempStore\TempStoreException
 */
function commerce_promo_tc_order_next_step_form_submit(array $form, FormStateInterface $form_state) {
  $coupon_redemption = $form_state->getValue('coupon_redemption');
  if (isset($coupon_redemption['form']['coupons']) && is_array($coupon_redemption['form']['coupons'])) {
    $tempstore = \Drupal::getContainer()->get('tempstore.private')->get('commerce_promo_tc');
    $tc_ids = [];
    foreach ($coupon_redemption['form']['coupons'] as $coupon) {
      $tc_id = $coupon['terms_and_conditions']['tc_check'];
      $tc_ids[$tc_id] = $tc_id;
      $tempstore->set('tc_accepted', $tc_ids);
    }
  }
}
