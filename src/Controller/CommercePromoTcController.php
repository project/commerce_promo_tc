<?php

namespace Drupal\commerce_promo_tc\Controller;

use Drupal\commerce_promo_tc\Entity\CommercePromoTc;
use Drupal\commerce_promotion\Entity\Promotion;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class CommercePromoTcController.
 *
 * @package Drupal\commerce_promo_tc\Controller
 */
class CommercePromoTcController extends ControllerBase {

  /**
   * The terms and conditions modal page.
   *
   * @param int $promotion
   *   The promotion id.
   *
   * @return array
   *   The page render array.
   */
  public function page($promotion) {
    $promotion = Promotion::load($promotion);
    $tc = CommercePromoTc::getTermsAndConditions($promotion);

    return [
      '#type' => 'markup',
      '#markup' => $tc->getText(),
      '#format' => $tc->getFormat(),
    ];
  }

}
