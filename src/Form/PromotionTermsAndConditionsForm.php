<?php

namespace Drupal\commerce_promo_tc\Form;

use Drupal\commerce_promo_tc\Entity\CommercePromoTc;
use Drupal\commerce_promotion\Form\PromotionForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PromotionTermsAndConditionsForm.
 *
 * @package Drupal\commerce_promo_tc\Form
 */
class PromotionTermsAndConditionsForm extends PromotionForm {

  /**
   * Perform specific submit steps for terms and conditions.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submit(array &$form, FormStateInterface $form_state) {
    $terms_and_condition = $form_state->getValue('terms_and_conditions');
    if (isset($terms_and_condition['value'])) {
      $form_obj = $form_state->getFormObject();
      $promotion = $form_obj->getEntity();

      // Check if terms and condition already exists for the promotion entity.
      $id = \Drupal::database()
        ->query('SELECT id FROM {commerce_promo_tc_field_data} WHERE commerce_promotion_id = :commerce_promotion_id AND langcode = :langcode', [
          ':commerce_promotion_id' => $promotion->id(),
          ':langcode' => $promotion->language()->getId(),
        ])->fetchCol();

      if (count($id) && array_values($id)[0]) {
        $entity = CommercePromoTc::load(array_values($id)[0]);
        $entity->set('value', $terms_and_condition['value']);
        $entity->set('format', $terms_and_condition['format']);
      }
      else {
        $entity = CommercePromoTc::create([
          'commerce_promotion_id' => $promotion->id(),
          'value' => $terms_and_condition['value'],
          'format' => $terms_and_condition['format'],
          'langcode' => $promotion->language(),
        ]);
      }

      $entity->save();
    }
  }

}
