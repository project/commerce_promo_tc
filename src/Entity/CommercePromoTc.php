<?php

namespace Drupal\commerce_promo_tc\Entity;

use Drupal\commerce_promotion\Entity\Promotion;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the CommercePromoTc commerce log entity.
 *
 * @ingroup commerce
 *
 * @ContentEntityType(
 *   id = "commerce_promo_tc",
 *   label = @Translation("Commerce Promotion Terms and Conditions"),
 *   handlers = {
 *     "access" = "Drupal\commerce_promo_tc\CommercePromoTcAccessControlHandler",
 *     "view_builder" = "Drupal\commerce_promo_tc\LogViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "commerce_promo_tc",
 *   translatable = TRUE,
 *   admin_permission = "administer commerce promotion terms and permissions",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "commerce_promotion_id" = "commerce_promotion_id"
 *   },
 * )
 */
class CommercePromoTc extends ContentEntityBase implements CommercePromoTcInterface {

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getText() {
    return $this->get('value')[0] ? $this->get('value')[0]->getValue()['value'] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormat() {
    return $this->get('format')[0] ? $this->get('format')[0]->getValue()['format'] : '';
  }

  /**
   * {@inheritdoc}
   */
  public static function getTermsAndConditions(Promotion $promotion) {
    $id = \Drupal::database()
      ->query('SELECT id FROM {commerce_promo_tc_field_data} WHERE commerce_promotion_id = :commerce_promotion_id AND langcode = :langcode', [
        ':commerce_promotion_id' => $promotion->id(),
        ':langcode' => $promotion->language()->getId(),
      ])->fetchCol();

    if (count($id) && array_values($id)[0]) {
      return CommercePromoTc::load(array_values($id)[0]);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['commerce_promotion_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Promotion'))
      ->setDescription(t('The promotion related the terms and conditions.'))
      ->setSetting('target_type', 'commerce_promotion')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'commerce_promotion',
        'weight' => 2,
      ]);

    $fields['value'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Value'))
      ->setDescription(t('Terms and conditions.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 6,
      ]);

    $fields['format'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Format'))
      ->setDescription(t('Value format.'));

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The terms and conditions entity language code.'))
      ->setRevisionable(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the terms and conditions was created.'))
      ->setTranslatable(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the terms and conditions was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

}
