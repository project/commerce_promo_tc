<?php

namespace Drupal\commerce_promo_tc\Entity;

use Drupal\commerce_promotion\Entity\Promotion;

/**
 * Provides an interface for defining CommercePromoTc log entities.
 *
 * @ingroup commerce_promo_tc
 */
interface CommercePromoTcInterface {

  /**
   * Gets the CommercePromoTccreation timestamp.
   *
   * @return int
   *   Creation timestamp of the CommercePromoTc.
   */
  public function getCreatedTime();

  /**
   * Sets the CommercePromoTc creation timestamp.
   *
   * @param int $timestamp
   *   The CommercePromoTc creation timestamp.
   *
   * @return \Drupal\commerce_promo_tc\Entity\CommercePromoTc
   *   The called CommercePromoTc entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the terms and condition text.
   *
   * @return string
   *   The terms and condition text.
   */
  public function getText();

  /**
   * Gets the terms and condition format.
   *
   * @return string
   *   The terms and condition format.
   */
  public function getFormat();

  /**
   * Gets the Terms and condition for a given promotion.
   *
   * @param \Drupal\commerce_promotion\Entity\Promotion $promotion
   *   The commerce promotion.
   *
   * @return \Drupal\commerce_promo_tc\Entity\CommercePromoTc|null
   *   The CommercePromoTc entity.
   */
  public static function getTermsAndConditions(Promotion $promotion);

}
